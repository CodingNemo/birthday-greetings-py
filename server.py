import connexion

import api
import resources
from definitions import SWAGGER_DIR


def create_app():
    flask_app = connexion.FlaskApp(__name__, specification_dir=SWAGGER_DIR)

    resources.init_app(flask_app)

    api.init_app(flask_app)

    return flask_app


flask_app = create_app()

if __name__ == '__main__':
    flask_app.run(port=8080)
