from smtplib import SMTP

from resources.directory import load_persons_from_csv
from resources.emails_sender import send_email


def init_app(app):
    app.app.directory = lambda: load_persons_from_csv("directory.csv")
    app.app.email_sender = lambda email: send_email(email, lambda: SMTP("localhost"))
