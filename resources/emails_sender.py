from email import message
from email.mime.text import MIMEText
from smtplib import SMTP, SMTPHeloError, SMTPRecipientsRefused, SMTPSenderRefused, SMTPConnectError
from typing import Callable

from domain.emails import Email, EmailNotSent


def send_email(email: Email, build_smtp_client: Callable[[], SMTP]) -> None:
    smtp_client = __connect_to_smtp_server(build_smtp_client)
    __sent_email_to_smtp_server(email, smtp_client)


def __sent_email_to_smtp_server(email, smtp_client):
    msg = __build_message(email)
    try:
        smtp_client.send_message(msg)
    except SMTPHeloError as err:
        raise __unreachable_server_error() from err
    except SMTPRecipientsRefused as err:
        recipients_errors = "\n".join(f" * {email_addr} : {error}" for (email_addr, error) in err.recipients.items())
        error_message = f"Could not contact recipients\n{recipients_errors}"
        raise EmailNotSent(error_message) from err
    except SMTPSenderRefused as err:
        raise EmailNotSent("Invalid 'From' email address : " + FROM_EMAIL_ADDRESS) from err
    except Exception as err:
        raise _unexpected_error(err) from err
    finally:
        smtp_client.quit()


def __connect_to_smtp_server(build_smtp_client):
    try:
        smtp_client = build_smtp_client()
    except ConnectionRefusedError as err:
        raise __unreachable_server_error() from err
    except SMTPConnectError as err:
        raise __unreachable_server_error() from err
    except Exception as err:
        raise _unexpected_error(err) from err
    return smtp_client


def __unreachable_server_error():
    return EmailNotSent("Could not reach SMTP server")


def _unexpected_error(err):
    return EmailNotSent(f"An unexpected error occured : '{err}'")


FROM_EMAIL_ADDRESS = "mail.sender@sample.com"


def __build_message(email: Email) -> message.Message:
    text_message = MIMEText(email.body)
    text_message["Subject"] = email.subject
    text_message["From"] = FROM_EMAIL_ADDRESS
    text_message["To"] = ",".join(sorted(email.to))
    return text_message
