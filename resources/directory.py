import csv
from datetime import datetime
from typing import List

from definitions import ROOT_DIR
from domain.directory import Person, InvalidDirectoryError


def __parse_row(row) -> Person:
    first_name = row["first_name"]
    last_name = row["last_name"]
    birthday = row["birthday"]
    email = row["email"]

    return Person(first_name, last_name, datetime.strptime(birthday, "%d/%m/%Y").date(), email)


def __check_row(row, reader):
    if len(row.keys()) != len(reader.fieldnames):
        raise InvalidDirectoryError(
            f"Could not retrieve directory from csv, line {reader.line_num}. Expected {len(reader.fieldnames)} "
            f"values. Found {len(row.keys())}")


def _open_csv_file(file_path: str):
    file_path = __fix_path(file_path)
    return open(file_path, "r")


def __fix_path(file_path):
    if not file_path.startswith(ROOT_DIR):
        if not file_path.startswith("/"):
            file_path = "/" + file_path
        file_path = ROOT_DIR + file_path
    return file_path


def _load_persons_from_csv(open_csv_file) -> List[Person]:
    persons = []

    with open_csv_file() as file:
        reader = csv.DictReader(file)

        try:
            for row in reader:
                __check_row(row, reader)
                persons.append(__parse_row(row))

        except KeyError as err:
            raise InvalidDirectoryError(
                f"Could not retrieve directory from csv, line {reader.line_num}: 'Could not find column {err}' ") from err

        except csv.Error as err:
            raise InvalidDirectoryError(
                f"Could not retrieve directory from csv, line {reader.line_num}: '{err}' ") from err

    return persons


def load_persons_from_csv(file_path):
    return _load_persons_from_csv(lambda: _open_csv_file(file_path))
