import json

from flask.json import jsonify

from domain.directory import Person


class JsonPerson:
    def __init__(self, person: Person):
        self.fullname = person.full_name
        self.email = person.email
        self.birthday = str(person.birthday)


def __convert(directory):
    return {"persons": [JsonPerson(person).__dict__ for person in directory]}


def search():
    from flask import current_app
    directory = current_app.directory()
    return jsonify(__convert(directory)), 200
