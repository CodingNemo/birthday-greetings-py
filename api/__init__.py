import json
from datetime import date

from connexion import RestyResolver
from flask import Response


def __render_domain_errors(exception):
    return Response(response=json.dumps({'error': str(exception), 'error_type': str(type(exception).__name__)}),
                    status=503,
                    mimetype="application/json")


def init_app(app):
    from domain.greetings import greetings
    from domain.emails import EmailNotSent
    from domain.directory import InvalidDirectoryError

    app.add_api('greetings_api.yml', resolver=RestyResolver('api'))

    app.add_error_handler(EmailNotSent, __render_domain_errors)
    app.add_error_handler(InvalidDirectoryError, __render_domain_errors)

    def hexagon():
        greetings(
            app.app.directory,
            app.app.email_sender,
            date.today
        )

    app.app.greetings = hexagon
