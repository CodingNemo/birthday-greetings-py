import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

SWAGGER_DIR = ROOT_DIR + "/api/swagger"
