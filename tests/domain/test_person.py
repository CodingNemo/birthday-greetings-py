from datetime import date

from domain.directory import Person


def test_is_person_birthday_when_today_has_same_day_and_month():
    person = Person("John", "McClane", date(1955, 3, 19), "john.mcclane@sample.com")
    assert person.is_birthday(today=lambda: date(2018, 3, 19)) is True


def test_is_not_person_birthday_when_today_has_not_same_day_or_month():
    person = Person("Frankie", "Vincent", date(1956, 4, 18), "frankie.vincent@sample.com")
    assert person.is_birthday(today=lambda: date(2018, 3, 19)) is False


def test_two_person_are_equal_when_same_firstname_lastname_birthday_email():
    person1 = Person("Frankie", "Vincent", date(1956, 4, 18), "frankie.vincent@sample.com")
    person2 = Person("Frankie", "Vincent", date(1956, 4, 18), "frankie.vincent@sample.com")

    assert person1 == person2


def test_two_person_are_not_equal_when_different_firstname_lastname_birthday_email():
    person1 = Person("Frankie", "Vincent", date(1956, 4, 18), "frankie.vincent@sample.com")
    person2 = Person("John", "McClane", date(1955, 3, 19), "john.mcclane@sample.com")

    assert person1 != person2


def test_person_is_printable():
    person = Person("John", "McClane", date(1955, 3, 19), "john.mcclane@sample.com")

    assert "John MCCLANE - 19/03/1955 - john.mcclane@sample.com" == str(person)
    assert "Person(first_name='John', last_name='McClane', birthday=datetime.date(1955, 3, 19), email='john.mcclane@sample.com')" == repr(
        person)