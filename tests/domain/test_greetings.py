from datetime import date

from domain.emails import Email
from domain.greetings import greetings
from tests.person_factory import mimie_mathy, michel_berger, graham_bell


def test_greetings_sends_a_happy_birthday_mail_to_persons_born_today():
    persons = [graham_bell(), michel_berger()]

    emails_sent = []
    send_email = lambda email: emails_sent.append(email)

    greetings(lambda: persons, send_email, today=lambda: date(2018, 8, 2))

    assert happy_birthday_graham_bell_email() in emails_sent
    assert happy_birthday_michel_berger_email() in emails_sent


def test_greetings_does_not_sends_a_happy_birthday_mail_to_person_not_born_today():
    persons = [graham_bell(), mimie_mathy()]

    emails_sent = []
    send_email = lambda email: emails_sent.append(email)

    greetings(lambda: persons, send_email, today=lambda: date(2018, 10, 3))

    assert 0 == len(emails_sent)


def test_greetings_sends_a_happy_birthday_reminder_mail_to_all_other_person_not_born_today():
    persons = [
        graham_bell(),
        michel_berger(),
        mimie_mathy(),
    ]

    emails_sent = []
    send_email = lambda email: emails_sent.append(email)

    greetings(lambda: persons, send_email, today=lambda: date(2018, 8, 2))

    reminder_email = Email({"mimie.mathy@sample.com"},
                           "Birthday reminder",
                           "Today is the birthday of Alexander Graham BELL and Michel BERGER !")
    assert reminder_email in emails_sent


def happy_birthday_michel_berger_email():
    return Email({"michel.berger@sample.com"}, "Happy birthday", "Happy Birthday Michel BERGER !!")


def happy_birthday_graham_bell_email():
    return Email({"alexander-graham.bell@sample.com"}, "Happy birthday", "Happy Birthday Alexander Graham BELL !!")
