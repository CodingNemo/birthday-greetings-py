from domain.emails import __format_birthdays_names, Email


def test_two_emails_are_equal_if_they_have_the_same_body_and_to():
    email1 = Email({"john.mcclane@sample.com"}, "Hello", "Yippee-ki-yay !!")
    email2 = Email({"john.mcclane@sample.com"}, "Hello", "Yippee-ki-yay !!")
    assert email1 == email2


def test_two_emails_are_not_equal_if_they_have_not_the_same_body_or_to():
    email1 = Email({"tychus.findlay@sample.com"}, "Hell Yeah", "It's about time !")
    email2 = Email({"jim.raynor@sample.com"}, "Ready to roll", "You up for this, Darlin'?")
    assert email1 != email2


def test_an_email_can_be_printed():
    email = Email({"confucius@sample.com"}, "Quote of the day", "Silence is a true friend who never betrays.")

    assert "To: confucius@sample.com\nSubject:Quote of the day\nSilence is a true friend who never betrays." == str(email)
    assert "Email(to={'confucius@sample.com'}, subject='Quote of the day', " \
           "body='Silence is a true friend who never betrays.')" == repr(email)


def test_should_format_birthdays_persons_name_in_reminder_email():
    assert "Lorie" == __format_birthdays_names(["Lorie"])
    assert "Lorie, Kurt and Kevin" == __format_birthdays_names(["Lorie", "Kurt", "Kevin"])
