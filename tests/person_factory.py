from datetime import date

from domain.directory import Person


def mimie_mathy():
    return Person("Mimie", "Mathy", date(1957, 7, 8), "mimie.mathy@sample.com")


def michel_berger():
    return Person("Michel", "Berger", date(1947, 8, 2), "michel.berger@sample.com")


def graham_bell():
    return Person("Alexander Graham", "Bell", date(1922, 8, 2), "alexander-graham.bell@sample.com")