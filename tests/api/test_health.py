def test_health(http_client):
    response = http_client.get('/api/v1/health')
    assert 200 == response.status_code
