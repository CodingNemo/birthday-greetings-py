import json
from unittest.mock import patch

from tests.person_factory import mimie_mathy


def test_directory_returns_a_list_of_persons(http_client):
    from server import flask_app

    with patch.object(flask_app.app, "directory") as mock_directory:
        mock_directory.return_value = [mimie_mathy()]

        response = http_client.get('/api/v1/directory')

        assert 200 == response.status_code
        mock_directory.assert_called()

        payload = json.loads(response.data)

        assert "persons" in payload
        assert payload["persons"] == [
            {"fullname": "Mimie MATHY",
             "email": "mimie.mathy@sample.com",
             "birthday": "1957-07-08"}]
