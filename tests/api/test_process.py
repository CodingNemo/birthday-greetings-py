from unittest.mock import patch


def test_process(http_client):
    from server import flask_app

    with patch.object(flask_app.app, "greetings") as mock_greetings:
        response = http_client.post('api/v1/process')
        assert 201 == response.status_code
        mock_greetings.assert_called()
