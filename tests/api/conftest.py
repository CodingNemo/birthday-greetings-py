import pytest

from server import flask_app


@pytest.fixture(scope='module')
def http_client():
    with flask_app.app.test_client() as c:
        yield c
