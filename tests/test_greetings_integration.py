from datetime import date

import pytest

from domain.greetings import greetings
from resources.directory import load_persons_from_csv
from resources.emails_sender import send_email

from tests.smtp_factory import create_smtp_client_for


@pytest.mark.integration
def test_greetings_with_real_directory_and_embedded_smtp_server_sends_mails(smtpserver):
    greetings(
        lambda: load_persons_from_csv("/tests/sample_directory.csv"),
        lambda email: send_email(email, lambda: create_smtp_client_for(smtpserver)),
        lambda: date(2018, 12, 30)
    )

    assert 2 == len(smtpserver.outbox)
