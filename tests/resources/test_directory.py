from io import StringIO

import pytest

from domain.directory import InvalidDirectoryError
from resources.directory import _load_persons_from_csv, load_persons_from_csv
from tests.person_factory import mimie_mathy, graham_bell, michel_berger


def test_load_persons_from_csv_file():
    file_content = """first_name,last_name,birthday,email
Mimie,Mathy,08/07/1957,mimie.mathy@sample.com
Alexander Graham,Bell,02/08/1922,alexander-graham.bell@sample.com
"""

    persons = _load_persons_from_csv(lambda: StringIO(file_content))

    assert len(persons) == 2
    assert mimie_mathy() in persons
    assert graham_bell() in persons


def test_load_persons_from_csv_file_raise_an_error_when_csv_is_badly_formatted():
    file_content = """first_name,last_name,birthday,email
Mimie,Mathy,08/07/1957,,mimie.mathy@sample.com
"""

    with pytest.raises(InvalidDirectoryError) as exceptionInfo:
        _load_persons_from_csv(lambda: StringIO(file_content))

    assert "Could not retrieve directory from csv, line 2. Expected 4 values. Found 5" == exceptionInfo.value.message


@pytest.mark.integration
@pytest.mark.timeout(60)
def test_load_persons_from_a_real_csv_file():
    persons = load_persons_from_csv("tests/resources/fake_directory.csv")

    assert len(persons) == 3
    assert mimie_mathy() in persons
    assert graham_bell() in persons
    assert michel_berger() in persons
