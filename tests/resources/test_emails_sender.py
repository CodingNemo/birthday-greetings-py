from email.mime.text import MIMEText
from smtplib import SMTPHeloError, SMTPRecipientsRefused, SMTPSenderRefused, SMTPConnectError

import pytest

from domain.emails import Email, EmailNotSent
from resources.emails_sender import send_email, __build_message, FROM_EMAIL_ADDRESS
from tests.smtp_factory import create_smtp_client_for


@pytest.fixture
def mock_smtp(mocker):
    return mocker.patch('smtplib.SMTP')


def test_send_email_to_a_smtp_server(mock_smtp):
    send_email(dummy_email(), lambda: mock_smtp)

    mock_smtp.send_message.assert_called_once()
    mock_smtp.quit.assert_called_once()


def dummy_email():
    return Email(to={"someguy@sample.com"}, subject="hey buddy !", body="Been a while !!")


def test_build_a_valid_message():
    email = Email(to={'to1@sample.com', 'to2@sample.com'},
                  subject="Something to talk about",
                  body="We need to talk, call me back !")

    message = __build_message(email)

    expected_content = '''Content-Type: text/plain; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Subject: Something to talk about
From: mail.sender@sample.com
To: to1@sample.com,to2@sample.com

We need to talk, call me back !'''

    assert isinstance(message, MIMEText)
    assert expected_content == str(message)


@pytest.mark.integration
@pytest.mark.timeout(60)
def test_send_email_to_an_embedded_smtp_server(smtpserver):
    email = Email(to={"someguy@sample.com"}, subject="hey buddy !", body="Been a while !!")
    send_email(email, lambda: create_smtp_client_for(smtpserver))
    assert len(smtpserver.outbox) == 1

    received_email = smtpserver.outbox[0]

    expected_mail = """Content-Type: text/plain; charset="us-ascii"
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Subject: hey buddy !
From: mail.sender@sample.com
To: someguy@sample.com

Been a while !!"""

    assert expected_mail == str(received_email)


def test_should_raise_an_email_not_sent_error_when_smtp_server_refused_the_connection():
    def build_unreachable_smtp_client():
        raise ConnectionRefusedError()

    with pytest.raises(EmailNotSent) as exceptionInfo:
        send_email(dummy_email(), build_unreachable_smtp_client)

    assert "Could not reach SMTP server" == exceptionInfo.value.message


def test_should_raise_an_email_not_sent_error_when_failing_to_contact_smtp_server():
    def build_unreachable_smtp_client():
        raise SMTPConnectError(421, "Could not connect")

    with pytest.raises(EmailNotSent) as exceptionInfo:
        send_email(dummy_email(), build_unreachable_smtp_client)

    assert "Could not reach SMTP server" == exceptionInfo.value.message


def test_should_raise_an_email_not_sent_error_when_failing_to_contact_smtp_server_when_sending_a_message(mock_smtp):
    mock_smtp.send_message.side_effect = SMTPHeloError(421, 'Service not available')

    with pytest.raises(EmailNotSent) as exceptionInfo:
        send_email(dummy_email(), lambda: mock_smtp)

    assert "Could not reach SMTP server" == exceptionInfo.value.message


def test_should_raise_an_email_not_sent_error_when_another_error_occured_during_connection():
    def build_unreachable_smtp_client():
        raise Exception("Something really bad happened")

    with pytest.raises(EmailNotSent) as exceptionInfo:
        send_email(dummy_email(), build_unreachable_smtp_client)

    assert "An unexpected error occured : 'Something really bad happened'" == exceptionInfo.value.message


def test_should_raise_an_email_not_sent_error_when_recipients_are_refused(mock_smtp):
    mock_smtp.send_message.side_effect = SMTPRecipientsRefused({"someguy@sample.com": "Unknown email address"})

    with pytest.raises(EmailNotSent) as exceptionInfo:
        send_email(dummy_email(), lambda: mock_smtp)

    assert """Could not contact recipients
 * someguy@sample.com : Unknown email address""" == exceptionInfo.value.message


def test_should_raise_an_email_not_sent_error_when_from_email_is_invalid(mock_smtp):
    mock_smtp.send_message.side_effect = SMTPSenderRefused(451, 'Sender email does not exist', FROM_EMAIL_ADDRESS)

    with pytest.raises(EmailNotSent) as exceptionInfo:
        send_email(dummy_email(), lambda: mock_smtp)

    assert "Invalid 'From' email address : " + FROM_EMAIL_ADDRESS == exceptionInfo.value.message


def test_should_raise_an_email_not_sent_error_when_another_error_occured_when_sending_an_email(mock_smtp):
    mock_smtp.send_message.side_effect = Exception("Something bad happened")

    with pytest.raises(EmailNotSent) as exceptionInfo:
        send_email(dummy_email(), lambda: mock_smtp)

    assert "An unexpected error occured : 'Something bad happened'" == exceptionInfo.value.message
