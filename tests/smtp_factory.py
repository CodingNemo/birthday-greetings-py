from smtplib import SMTP


def create_smtp_client_for(smtp_server):
    return SMTP(smtp_server_host(smtp_server), smtp_server_port(smtp_server))


def smtp_server_host(smtp_server):
    return smtp_server.addr[0]


def smtp_server_port(smtp_server):
    return smtp_server.addr[1]
