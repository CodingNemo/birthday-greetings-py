from datetime import date
from typing import Callable, List

from domain.directory import Person
from domain.emails import happy_birthday_email, birthday_reminder, Email


def greetings(load_persons: Callable[[], List[Person]], send_email: Callable[[Email], None],
              today: Callable[[], date]) -> None:
    persons = load_persons()

    birthday_persons = [person for person in persons if person.is_birthday(today)]
    not_birthday_persons = [person for person in persons if not person.is_birthday(today)]

    __send_happy_birthday_emails(birthday_persons, send_email)

    __send_birthday_reminder_email(birthday_persons, not_birthday_persons, send_email)


def __send_birthday_reminder_email(birthday_persons: List[Person], not_birthday_persons: List[Person],
                                   send_email: Callable[[Email], None]) -> None:
    if len(birthday_persons) > 0:
        send_email(birthday_reminder(birthday_persons, not_birthday_persons))


def __send_happy_birthday_emails(birthday_persons: List[Person], send_email: Callable[[Email], None]) -> None:
    for person in birthday_persons:
        send_email(happy_birthday_email(person))
