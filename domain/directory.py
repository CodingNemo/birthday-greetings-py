import datetime
from typing import Callable, Any


class Person:
    def __init__(self, first_name: str, last_name: str, birthday: datetime.date, email: str) -> None:
        self.__first_name = first_name
        self.__last_name = last_name
        self.__email = email
        self.__birthday = birthday

    @property
    def full_name(self) -> str:
        return self.__first_name + " " + self.__last_name.upper()

    @property
    def email(self) -> str:
        return self.__email

    @property
    def birthday(self) -> datetime.date:
        return self.__birthday

    def is_birthday(self, today: Callable[[], datetime.date]) -> bool:
        today_date = today()
        return self.birthday.month == today_date.month \
               and self.birthday.day == today_date.day

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, Person) and \
               self.full_name == other.full_name and \
               self.birthday == other.birthday and \
               self.email == other.email

    def __str__(self) -> str:
        formatted_date = self.birthday.strftime("%d/%m/%Y")
        return f'{self.full_name} - {formatted_date} - {self.email}'

    def __repr__(self) -> str:
        return f"Person(first_name='{self.__first_name}', last_name='{self.__last_name}', " \
               f"birthday={repr(self.birthday)}, email='{self.email}')"


class InvalidDirectoryError(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.message = message