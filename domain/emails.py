from typing import Set, Any, List

from domain.directory import Person


class Email:
    def __init__(self, to: Set[str], subject: str, body: str) -> None:
        self.to = to
        self.subject = subject
        self.body = body

    def __eq__(self, other: Any) -> bool:
        return isinstance(other, Email) \
               and self.to == other.to \
               and self.subject == other.subject \
               and self.body == other.body

    def __str__(self) -> str:
        to_as_str = ",".join(self.to)
        return f"To: {to_as_str}\nSubject:{self.subject}\n{self.body}"

    def __repr__(self) -> str:
        return f"Email(to={self.to}, subject='{self.subject}', body='{self.body}')"


def happy_birthday_email(person: Person) -> Email:
    return Email({person.email}, "Happy birthday", f"Happy Birthday {person.full_name} !!")


def birthday_reminder(birthday_persons: List[Person], not_birthday_persons: List[Person]) -> Email:
    to = {person.email for person in not_birthday_persons}
    birthdays_names = [person.full_name for person in birthday_persons]

    body = f"Today is the birthday of {__format_birthdays_names(birthdays_names)} !"
    return Email(to, "Birthday reminder", body)


def __format_birthdays_names(birthdays_names: List[str]) -> str:
    if len(birthdays_names) == 1:
        return birthdays_names[0]

    return ", ".join(birthdays_names[:-1]) + " and " + birthdays_names[-1]


class EmailNotSent(Exception):
    def __init__(self, message):
        super().__init__(message)
        self.message = message
